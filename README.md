<!-- writeme -->
Bulk Invite
===========

Administrators can invite multiple people to their site at once (with user accounts created for them.

 * https://www.drupal.org/project/bulk_invite
 * Issues: https://www.drupal.org/project/issues/bulk_invite
 * Source code: https://git.drupalcode.org/project/bulk_invite
 * Package name: drupal/bulk_invite


### Requirements

 * mmucklo/email-parse ^0.4.3


### License

GPL-2.0+

<!-- endwriteme -->
